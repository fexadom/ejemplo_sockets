CC = gcc
CFLAGS = -Wall -I .

LIB = -lpthread

.PHONY: all clean

all: client server

client: echoclient.c csapp.o
	$(CC) $(CFLAGS) -o client echoclient.c csapp.o $(LIB)

server: echoserveri.c csapp.o
	$(CC) $(CFLAGS) -o server echoserveri.c csapp.o $(LIB)

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c csapp.c

clean:
	rm -f *.o client server *~

